import { Component } from '@angular/core';
import { Platform, App, NavController,
         AlertController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { NativeStorage } from '@ionic-native/native-storage';

import { LoginPage } from '../pages/login/login';

import { ImagePicker } from '@ionic-native/image-picker';

@Component({
  templateUrl: 'app.html',
  providers: [ ImagePicker ]
})
export class MyApp {
  rootPage:any = LoginPage;
  avatar = "assets/imgs/default_profile.jpeg";
  code = '';
  name = "";

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              public app: App,
              public imagePicker: ImagePicker,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              public nativeStorage: NativeStorage) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.nativeStorage.getItem('student').then(
      data => {
        this.code = data.code;
        this.name = data.name;
        if(data.img) {
          this.avatar = "data:image/png;base64," + data.img;
        }
      }, error => {}
    );
  }

  takePicture(){
    const options = {
      maximumImagesCount: 1,
      quality: 50,
      width: 512,
      height: 512,
      outputType: 1
    }
    this.imagePicker.getPictures(options).then((imageData) => {
      this.avatar = "data:image/png;base64," + imageData[0];
      this.nativeStorage.setItem('student', {
        code: this.code,
        name: this.name,
        img: imageData[0]
      });
    }, (err) => {
      console.log(err);
    });
  }

  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'ALERTA',
      message: '¿Desea desvincular su cuenta? Se eliminarán todos sus datos.',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {}
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.nativeStorage.remove('student').then(
              () => {
                let _t = this;
                let del = new XMLHttpRequest();
                del.onreadystatechange = function() {
                  if(del.readyState == 4 && del.status == 200) {
                    let navCtrl: NavController = _t.app.getRootNav();
                    navCtrl.setRoot(LoginPage);
                  }
                }
                let delStr = "https://rebeca290197.000webhostapp.com/del_student.php";
                delStr += '?code=' + _t.code;
                del.open('GET', delStr, true);
                del.send();
              }, error => {
                let toast = this.toastCtrl.create({
                  message: '¡Error, inténtelo más tarde!',
                  duration: 2500
                });
                toast.present();
              }
            )
          }
        }
      ]
    });
    confirm.present();
  }

};
