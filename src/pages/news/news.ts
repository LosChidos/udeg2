import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';

import { NewsDetailsPage } from '../news-details/news-details';

@Component({
  selector: 'page-news',
  templateUrl: 'news.html'
})
export class NewsPage {

  news = [];

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController) {
    this.fetchNews()
  }

  fetchNews() {
    let xhttp = new XMLHttpRequest();
      let _this = this;
      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
          _this.news = JSON.parse(xhttp.responseText);
        }
      };
      let qStr = 'https://rebeca290197.000webhostapp.com/get_news.php';
      xhttp.open('GET', qStr, true);
      xhttp.send();
  }

  doRefresh(refresher) {
    setTimeout(() => {
      let xhttp = new XMLHttpRequest();
        let _t = this;
        xhttp.onreadystatechange = function() {
          if (xhttp.readyState == 4 && xhttp.status == 200) {
            let res = JSON.parse(xhttp.responseText);
            _t.news = res.news;
            refresher.complete();
          }
        };
        let qStr = 'https://rebeca290197.000webhostapp.com/api.php/news?transform=1';
        xhttp.open('GET', qStr, true);
        xhttp.send();
    }, 2000);
  }

  openNew(n) {
    let modal = this.modalCtrl.create(NewsDetailsPage, {'new': n});
    modal.present();
  }
}
