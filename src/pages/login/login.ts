import { Component } from '@angular/core';
import { NavController, MenuController,
         ModalController, ToastController } from 'ionic-angular';

import { NativeStorage } from '@ionic-native/native-storage';

import { AboutPage } from '../about/about';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  code = "";
  nip = "";

  constructor(public navCtrl: NavController,
              public menuCtrl: MenuController,
              public modalCtrl: ModalController,
              public toastCtrl: ToastController,
              public nativeStorage: NativeStorage) {
    this.nativeStorage.getItem('student').then(
      data => {
        this.navCtrl.setRoot(TabsPage, {'student': data.code})
      }, error => {
        this.menuCtrl.enable(false);
      }
    );
  }

  showAbout() {
    let aboutModal = this.modalCtrl.create(AboutPage);
    aboutModal.present();
  }

  logIn() {
    let xhttp = new XMLHttpRequest();
    let _t = this;
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        let res = xhttp.responseText.split(',');
        if(res[0] == "0" || res[0] == "T") {
          let toast = _t.toastCtrl.create({
            message: '¡Credenciales incorrectas!',
            duration: 2500
          });
          toast.present();
        } else {
          _t.menuCtrl.enable(true);
          _t.nativeStorage.setItem('student', {
            code: _t.code,
            name: res[2]
          }).then(
            () => {
                let add = new XMLHttpRequest();
                add.onreadystatechange = function() {
                  if(add.readyState == 4 && add.status == 200) {
                    _t.navCtrl.setRoot(TabsPage, {'student': _t.code});
                  }
                }
                let addStr = "https://rebeca290197.000webhostapp.com/new_student.php";
                addStr += '?code=' + res[1] + '&name=' + encodeURI(res[2]) +
                          '&car=' + encodeURI(res[4]);
                add.open('GET', addStr, true);
                add.send();
            }, error => {
              let toast = _t.toastCtrl.create({
                message: '¡Problema con NativeStorage!',
                duration: 2500
              });
              toast.present();
            }
          );
        }
      }
    };
    xhttp.open("POST", "http://dcc.000webhostapp.com/pruebaLogin.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("codigo=" + this.code + "&nip=" + this.nip);
  }
}
