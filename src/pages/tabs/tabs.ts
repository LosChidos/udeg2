import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';
import { NewsPage } from '../news/news';
import { SchedulePage } from '../schedule/schedule';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  homeTab = HomePage;
  newsTab = NewsPage;
  scheduleTab = SchedulePage;

  student = "";

  constructor(public navParams: NavParams) {
    this.student = this.navParams.get('student');
  }
}
