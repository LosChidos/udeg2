import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-news-details',
  templateUrl: 'news-details.html',
})
export class NewsDetailsPage {

  data = {};

  constructor(public navCtrl: NavController,
              public navParams: NavParams) {
    this.data = navParams.get('new');
  }

  dismiss() {
    this.navCtrl.pop();
  }
}
