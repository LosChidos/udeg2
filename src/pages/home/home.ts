import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  weatherTemp = 0;
  weatherTime = "";
  weatherIcon = "wi-yahoo-3200";

  imgs = [];

  constructor(public navCtrl: NavController) {
    this.fetchWeather();
    this.fetchImgs();
  }

  fetchWeather() {
    let xhttp = new XMLHttpRequest();
    let _this = this;
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        let res = JSON.parse(xhttp.responseText);
        let weather = res.query.results.channel.item.condition;
        _this.weatherIcon = "wi-yahoo-" + weather.code;
        _this.weatherTemp = weather.temp;
        _this.weatherTime = weather.date.substr(weather.date.indexOf(':') - 2);
      }
    };
    let qStr = 'https://query.yahooapis.com/v1/public/yql?q=';
    qStr += encodeURI('select item.condition from weather.forecast ');
    qStr += encodeURI('where woeid=124162 and u="c"');
    qStr += encodeURI('&format=json');
    xhttp.open('GET', qStr, true);
    xhttp.send();
  }

  fetchImgs() {
    let xhttp = new XMLHttpRequest();
    let _this = this;
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        _this.imgs = JSON.parse(xhttp.responseText);
      }
    };
    let qStr = 'https://rebeca290197.000webhostapp.com/get_images.php';
    xhttp.open('GET', qStr, true);
    xhttp.send();
  }
}
