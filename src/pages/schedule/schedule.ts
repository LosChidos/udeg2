import { Component } from '@angular/core';
import { NavController, NavParams,
         ModalController, AlertController,
         ToastController } from 'ionic-angular';

@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html'
})
export class SchedulePage {

  currDay = "1";
  student = "";
  sections = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController) {
    let currDate = new Date();
    this.currDay = currDate.getDay().toString();
    if(this.currDay == "0") {
      this.currDay = "1";
    }
    this.student = this.navParams.data;
    this.fetchSections(this.student);
  }

  fetchSections(student) {
    let xhttp = new XMLHttpRequest();
      let _this = this;
      _this.sections = [];
      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
          console.log(xhttp.responseText);
          let res = JSON.parse(xhttp.responseText).students_sections;
          for(let s of res) {
            console.log(s);
            _this.sections.push({
                time: s.sections[0].time,
                place: s.sections[0].build,
                subject: s.sections[0].subject,
                days: s.sections[0].days,
                teacher: s.sections[0].teachers[0].name,
                color: "#" + _this.intToRGB(_this.hashCode(s.sections[0].subject))
            })
            console.log(_this.sections);
          }
        }
      };
      let qStr = 'http://rebeca290197.000webhostapp.com/api.php/students_sections?transform=1&include=sections,teachers&filter=student,eq,';
      qStr += student;
      console.log(qStr);
      xhttp.open('GET', qStr, true);
      xhttp.send();
  }

  getDaySections(dayStr) {
    if(dayStr == '1') {
      dayStr = 'L';
    } else if(dayStr == '2') {
      dayStr = 'M';
    } else if(dayStr == '3') {
      dayStr = 'I';
    } else if(dayStr == '4') {
      dayStr = 'J';
    } else if(dayStr == '5') {
      dayStr = 'V';
    } else if(dayStr == '6') {
      dayStr = 'S';
    }
    return this.sections.filter(s => s.days.search(dayStr) != -1);
  }

  hashCode(str) {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
  }

  intToRGB(i) {
    let c = (i & 0x00FFFFFF).toString(16).toUpperCase();
    return "00000".substring(0, 6 - c.length) + c;
  }

  showToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2500
    });
    toast.present();
  }

  addNrc() {
    let prompt = this.alertCtrl.create({
      title: 'Agregar clase',
      message: "Ingresa el NRC correspondiente a la clase",
      inputs: [
        {
          name: 'nrc',
          placeholder: 'NRC'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {}
        },
        {
          text: 'Agregar',
          handler: data => {
            let _t = this;
            let exist = new XMLHttpRequest();
            let dup = new XMLHttpRequest();
            let ins = new XMLHttpRequest();
            exist.onreadystatechange = function() {
              if (exist.readyState == 4 && exist.status == 200) {
                if(exist.responseText == "1") {
                  let q = 'https://rebeca290197.000webhostapp.com/is_enrolled.php?';
                  q += "student=" + _t.student + "&section=" + data.nrc;
                  dup.open('GET', q, true);
                  dup.send();
                } else {
                  _t.showToast('¡Esa clase no existe!');
                }
              }
            };
            dup.onreadystatechange = function() {
              if (dup.readyState == 4 && dup.status == 200) {
                if(dup.responseText == "0") {
                  let i = 'https://rebeca290197.000webhostapp.com/enroll.php?';
                  i += "student=" + _t.student + "&section=" + data.nrc;
                  ins.open('GET', i, true);
                  ins.send();
                } else {
                  _t.showToast('¡Ya está registrado en esa clase!');
                }
              }
            };
            ins.onreadystatechange = function() {
              if (ins.readyState == 4 && ins.status == 200) {
                _t.fetchSections(_t.student);
              }
            };
            let existQ = 'https://rebeca290197.000webhostapp.com/nrc_exists.php?';
            existQ += "section=" + data.nrc;
            exist.open('GET', existQ, true);
            exist.send();
          }
        }
      ]
    });
    prompt.present();
  }

}
